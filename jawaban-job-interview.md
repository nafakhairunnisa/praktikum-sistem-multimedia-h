# No 1

Demo color detection:
![color-detection](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/color-detection-demo.gif)

[Link Source Code](https://github.com/nafakhairunnisa/color-detection-with-python.git)

# No 2

Program di atas adalah sebuah program yang melakukan kompresi gambar dan deteksi warna pada gambar menggunakan bahasa pemrograman Python.
Program ini menggunakan 3 library, di antaranya:
1. Pandas yaitu library python untuk melakukan manipulasi dan analisis data. Pada program ini pandas digunakan untuk menganalisis data warna pada file csv.
2. Cv2 atau opencv berfungsi untuk pengolahan citra atau gambar dan video.
3. Os yaitu library yang berfungsi untuk memanipulasi dan menampilkan informasi file di dalam direktori sebuah operasi sistem.

Cara kerja:
1. Pertama-tama user menginput gambar dan program akan membaca gambar tersebut dengan fungsi imread().
2. Selanjutnya program mengubah nama file original dengan menambahkan kata “_compressed” di akhir nama file
3. Gambar akan dikompresi oleh program dengan kualitas 50 dari rentang 1-100.
4. Program akan menampilkan ukuran awal file dan ukuran setelah dikompresi.
5. Selanjutnya, program akan menggunakan library pandas untuk membaca file csv yang berisi informasi tentang nama-nama warna dan kode hex, RGB.
6. Program akan menampilkan gambar yang dipilih tadi di jendela baru dengan fungsi namedWindow() dan imshow() dan menyesuaikan ukurannya menjadi 700 x 700 pixel.
7. Jika user mengeklik dua kali pada warna yang akan dideteksi, program akan menentukan warna dari piksel yang diklik menggunakan fungsi get_color_name(). Program akan mendeteksi nilai RGB dari warna tersebut dan mencari warna yang cocok dengan yang ada dalam file csv.
8. Sebuah persegi panjang yang di dalamnya terdapat nama warna, nilai RGB, dan kode hex pada gambar akan muncul.
9. Program akan berhenti jika user mengklik tombol ESC.

Flowchart program:
![Flowchart_cdp](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/color-detection-mermaid-flowchart.svg)

Flowchart algoritma color detection:
![Flowchart_cd](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/flowchart_csv_color_detection.svg)

Flowchart algoritma draw_function:
![Flowchart_df](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/flowchart_csv_draw_function.svg)

Flowchart algoritma get_color_name:
![Flowchart_gcn](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/flowchart_csv_get_color_name.svg)

[Link Youtube](https://youtu.be/pu3z9VcbhuU)

# No 3
Program ini tidak memiliki aspek kecerdasan buatan. Program ini merupakan aplikasi color detection sederhana yang menggunakan OpenCV dan pandas untuk melakukan kompresi gambar serta deteksi warna berdasarkan nilai piksel gambar yang diklik oleh pengguna. Program ini melakukan analisis data melalui image processing dan penggunaan data warna yang telah tersedia dalam file CSV. Program ini menggunakan algoritma sederhana pada color detection, draw_function, dan get_color_name yang tertera pada flowchart di nomor 2.

# No 4
Demo voice assistant:
![voice assistant](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/voice-assistant-demo.gif)

[Link Source Code](https://github.com/nafakhairunnisa/voice-assistant-with-python.git)

# No 5
Program ini adalah sebuah program yang berfungsi sebagai asisten suara yang dapat mengenali suara pengguna untuk mengeksekusi perintah. Program ini memiliki beberapa fitur, seperti mengubah bahasa, menjawab pertanyaan, memainkan musik di YouTube, mencari informasi di Wikipedia, dan mengompresi file audio.
Program ini menggunakan 7 library, di antaranya:
1. Speech_recognition, berfungsi untuk mengenali suara yang diinput dari device microphone dan mengubah suara menjadi teks (speech to text)
2. Pyttsx3, berfungsi untuk mengubah teks menjadi suara (text to speech)
3. Pywhatkit, berfungsi untuk memainkan video di youtube.
4. Datetime, berfungsi untuk menampilkan dan memanipulasi waktu.
5. Wikipedia, berfunsi untuk mencari informasi dari website Wikipedia.
6. Os, berfungsi untuk memanipulasi dan menampilkan informasi file di dalam direktori sebuah operasi sistem.
7. AudioSegment dari pydub, berfungsi untuk memanipulasi file audio.

Cara kerja:
1. Pertama-tama program akan berjalan dalam bahasa inggris sebagai bahasa default.
2. User memberikan command/perintah.
3. Program akan merespon dengan menjalankan perintah tertentu yang sudah disetting di dalam program. Jika perintah tidak dikenali, program akan mengucapkan “Plese say the command again” atau “Tolong ulangi perintahnya”.
4. Program akan berhenti jika user mengatakan “stop”, “exit”, atau “berhenti”.

![Flowchart](https://gitlab.com/nafakhairunnisa/praktikum-sistem-multimedia-h/-/raw/main/voice-assistant-mermaid-flowchart.svg)

[Link Youtube](https://youtu.be/L1VWw2qZ1YI)

# No 6
Program ini menggunakan beberapa aspek kecerdasan buatan (AI) seperti Speech Recognition, Text-to-Speech Synthesis, NLP (Natural Language Processing) dan pengambilan informasi dari Wikipedia. Selain itu, program ini juga mengandung beberapa fungsi yang dapat menyelesaikan tugas secara otomatis seperti mengubah bitrate dari file audio, memainkan lagu di YouTube menggunakan modul pywhatkit, dan menampilkan waktu saat ini.
